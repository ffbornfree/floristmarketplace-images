#!/bin/sh

# SET NGINX client_max_body_size
if [ ! -z "$NGINX_MAX_BODY_SIZE" ]
	then sed -i "s/<NGINX_MAX_BODY_SIZE>/$NGINX_MAX_BODY_SIZE/" /etc/nginx/nginx.conf
	else sed -i "s/<NGINX_MAX_BODY_SIZE>/5M/" /etc/nginx/nginx.conf
fi

# CREATE LINK
ln -s /var/www/html/storage/app/public/ /var/www/html/public/storage

# SET APACHE HOST FOR PROXY
if [ ! -z "$PHP_SERVICE_HOST" ]; then sed -i "s/<PHP_SERVICE_HOST>/$PHP_SERVICE_HOST/" /etc/nginx/nginx.conf; fi

# PRINT START TIME
echo "Start Nginx Webserver: $(date '+%Y-%m-%d %H:%M:%S')"

# EXEC NGINX DAEMON
exec nginx -g "daemon off; error_log /dev/stderr info;"
