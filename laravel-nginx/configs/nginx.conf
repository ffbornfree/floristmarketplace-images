user  root root;
worker_processes  auto;

pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    access_log off;
    access_log off;

    sendfile        on;
    tcp_nopush     on;

    keepalive_timeout  65;

    server_tokens off;

    gzip  on;
    gzip_comp_level  3;

    map $http_x_forwarded_proto $fastcgi_https {
        default off;
        https on;
    }

    server {
        listen   80 default_server;

        root /var/www/html/public;
        index index.php index.html index.htm;

        error_log off;

        location ~* ^.+\.(?:css|cur|js|jpe?g|gif|htc|ico|png|html|xml|otf|ttf|eot|woff|svg)$ {
            access_log off;
            expires 7d;

            tcp_nodelay off;

            open_file_cache max=3000 inactive=120s;
            open_file_cache_valid 45s;
            open_file_cache_min_uses 2;
            open_file_cache_errors off;
        }

        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

        location ~ \.php$ {
            try_files $uri = 404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass <PHP_SERVICE_HOST>:9000;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param SCRIPT_NAME $fastcgi_script_name;
            fastcgi_param HTTPS $fastcgi_https;
            fastcgi_index index.php;
            include fastcgi_params;
        }
    }
}
