#!/bin/sh

# RUN ARTISAN OPTIMIZE
php /var/www/html/artisan optimize

# PRINT START TIME
echo "Start Laravel Cron: $(date '+%Y-%m-%d %H:%M:%S')"

# START CROND
exec crond -f
